package io.project.toros.data;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import io.project.toros.model.Player;


public interface PlayerRepository extends JpaRepository<Player, Long> {

    Optional<Player> findById(Long id);
    
    Page<Player> findAll(Pageable pageable);

    Optional<Player> findByPhone(String phone);
    
    void delete(Player player);

}
