package io.project.toros.services;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.project.toros.data.PlayerRepository;
import io.project.toros.model.Player;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("/api/players")
public class PlayerService {

	@Autowired
	private PlayerRepository playerRepository;


	@GetMapping(value = "/")
	public Page<Player> list(Pageable pageable) {
		return playerRepository.findAll(pageable);
	}


	@PostMapping(value = "/")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Player create(@RequestBody @Valid  Player player) {
		log.info("Creating payer");
	
        return playerRepository.saveAndFlush(player);
	}

	@DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id) {
		log.info("Delete payer with id  - {}", id );

		Player player = playerRepository.findById(id)
				.orElseThrow(()-> new EntityNotFoundException("Customer"));
		
		playerRepository.delete(player);
    }

}
